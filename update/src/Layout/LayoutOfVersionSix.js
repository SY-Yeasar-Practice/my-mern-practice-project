import React, {useState} from 'react'
import {Routes, Route, Navigate, useNavigate} from 'react-router-dom'
import Home from '../ComponentVersionSix/Home/Home/Home'
import Navbar from '../ComponentVersionSix/NavBar/Navbar'
import DetailsLayout from '../ComponentVersionSix/Profile/Details/DetailsLayout'
import Details from '../ComponentVersionSix/Profile/Details/DetailsLayout'
import Family from '../ComponentVersionSix/Profile/Details/Family/Family'
import Photo from '../ComponentVersionSix/Profile/Photo/Photo'
import ProfileLayout from '../ComponentVersionSix/Profile/ProfileLayout'
import Personal from '../ComponentVersionSix/Profile/Details//Personal/Personal'

const LayoutOfVersionSix = () => {
    const [isLoggedIn, setIsLoggedIn ] = useState (false)
    const navigate = useNavigate()
    return (
        <div>
            <div>
                <nav>
                    <Navbar />
                </nav>
            <section className >
                    <Routes>
                        <Route path = {`/`}  element = {isLoggedIn ? <Navigate to = {`/profile`}/> : <Home/>  }/>
                        <Route path = {`/profile/*`} element = {<ProfileLayout/>} >
                            <Route path = {`details`} element = {<DetailsLayout/>} > 
                                <Route path = {`family`} element = {<Family/>} />
                                <Route path = {`personal`} element = {<Personal/>} />
                            </Route>
                            <Route path = {`photo`} element = {<Photo/>}/>
                        </Route>
                    </Routes>
            </section>
            <footer>
                <button type="button" onClick={(e) => navigate("/profile/details/personal", {relative: true})}>
                    Go home
                    </button>
            </footer>
            </div>
        </div>
    )
}

export default LayoutOfVersionSix
