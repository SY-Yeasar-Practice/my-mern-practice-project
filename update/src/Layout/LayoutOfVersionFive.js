import React from 'react'
import { Route, Switch, useHistory} from 'react-router'
import Home from '../Component/Home/Home'
import Navbar from '../Component/Navbar/Navbar'
import ProfileLayout from '../Component/Profile/ProfileLayout'

const Layout = () => {
    const history = useHistory()
    const handleClick = (e) => {
         history.goBack();
    }
    return (
        <div>
            <nav>
                <Navbar />
            </nav>
           <section className >
                <Switch>
                    <Route exact path = {`/`} component = {Home}/>
                    <Route path = {`/profile`}>
                        <ProfileLayout/>
                    </Route>
                </Switch>
           </section>
           <footer>
               <button type="button" onClick={handleClick}>
                Go home
                </button>
           </footer>
        </div>
    )
}

export default Layout
