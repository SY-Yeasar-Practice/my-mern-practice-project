import React from 'react'
import { NavLink } from 'react-router-dom'

const Navbar = ({url}) => {
    return (
        <div>
            <NavLink to = {`${url}/details`}>Details</NavLink>
            <br/>
            <NavLink to = {`${url}/photo`} k>Photo</NavLink>
            <br/>
            <NavLink to = {`${url}/blog`} >Blog</NavLink>
        </div>
    )
}

export default Navbar
