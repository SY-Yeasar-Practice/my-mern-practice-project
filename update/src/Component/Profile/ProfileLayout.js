import React from 'react'
import { useRouteMatch, Route, Routes } from 'react-router'
import Blog from './Blog/Blog'
import Details from './Details/DetailsLayout'
import Navbar from './NavBar/Navbar'
// import PageNotFound from '../PageNotFound/PageNotFound'
// import DetailsLayout from './Details/DetailsLayout'

const ProfileLayout = () => {
    return (
        <div>
            <br/>
            <br/>
            <Navbar url = {url}/>

            <Routes>
                <Route path = {"/"}>
                    <Blog/>
                </Route>
                <Route path = {`${path}/details`}>
                    {/* <DetailsLayout/> */}
                </Route>
                <Route path = {`${path}/blog`}>
                    <Blog/>
                </Route>
                <Route path = {`${path}/*`}>
                    {/* <PageNotFound/> */}
                </Route>
            </Routes>

        </div>
    )
}

export default ProfileLayout
