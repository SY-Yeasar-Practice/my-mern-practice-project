import React from 'react'
import { useRouteMatch, Route, Switch } from 'react-router'
import { NavLink } from 'react-router-dom'
import About from './About/About'
import FatherName from './FatherName/FatherName'
import Navbar from './NavBar.js/Navbar'

const DetailsLayout = ({routeUrl}) => {
    const {url, path} = useRouteMatch()
    return (
        <div>
            <Navbar />
            <Switch>
                <Route exact path = {`${path}`}>
                    <h1>Hello</h1>
                </Route>
                <Route  path = {`${path}/about`}>
                    <About/>
                </Route>
                <Route path = {`${path}/fatherName`}>
                    <FatherName/>
                </Route>
            </Switch> 
        </div>
    )
}

export default DetailsLayout
