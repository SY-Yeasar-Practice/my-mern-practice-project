import React from 'react'
import {NavLink, useRouteMatch} from 'react-router-dom'

const Navbar = () => {
    const {url, path} = useRouteMatch()
    return (
        <div>
            <br/>
            <NavLink to = {`${url}/about`}>About</NavLink>
            <br/>
            <NavLink to = {`${url}/fatherName`} >Father Name</NavLink>
           
        </div>
    )
}

export default Navbar
