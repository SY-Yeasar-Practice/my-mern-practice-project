import React from 'react'
import {Link, NavLink} from 'react-router-dom'

const Navbar = () => {
    return (
        <div>
            <NavLink to = "/">Home</NavLink>
            <br/>
            <NavLink to = "/profile">Profile</NavLink>
        </div>
    )
}

export default Navbar
