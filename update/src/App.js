import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router} from 'react-router-dom'
// import LayoutOfVersionFive from './Layout/LayoutOfVersionFive'
import LayoutOfVersionSix from './Layout/LayoutOfVersionSix'
function App() {
  return (
    <body className="">
        <Router>
           {/* <LayoutOfVersionFive/> */}
           <LayoutOfVersionSix/>
        </Router>
    </body>
  );
}

export default App;
