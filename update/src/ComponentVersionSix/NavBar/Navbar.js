import React from 'react'
import {Link, NavLink} from 'react-router-dom'
import NavBarStyleSheet from './NavBar.module.css'

const Navbar = () => {
    return (
        <div>
            <NavLink className = {(nav) => nav.isActive ? NavBarStyleSheet.active : NavBarStyleSheet.nav } to = "/">Home</NavLink>
            <br/>
            <NavLink  className = {(nav) => nav.isActive ? NavBarStyleSheet.active : NavBarStyleSheet.nav}to = "/profile">Profile</NavLink>
        </div>
    )
}

export default Navbar
