import React from 'react'
import { Outlet, Route, Routes } from 'react-router'
import Details from './Details/DetailsLayout'
import Navbar from './Navbar/Navbar'

const ProfileLayout = () => {
    return (
        <div>
            <br/>
            <br/>
            <Navbar/>
            <Outlet/>
        </div>
    )
}

export default ProfileLayout
