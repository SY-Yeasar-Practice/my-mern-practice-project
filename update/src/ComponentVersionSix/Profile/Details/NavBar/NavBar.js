import React from 'react'
import { NavLink } from 'react-router-dom'

const NavBar = () => {
    return (
        <div>
            <br/>
            <NavLink to = {`personal`}>Personal</NavLink>
            <br/>
            <NavLink to = {`family`} >Family</NavLink>
            
        </div>
    )
}

export default NavBar
