import React from 'react'
import PersonalInfo from './PersonalInfo/PersonalInfo'
import ProfileStylesheet from './Profile.module.css'
import ShortDescription from './ShortDescription/ShortDescription'

const Profile = () => {
    return (
        <div className= {`student-profile py-4 ${ProfileStylesheet.wrapper}`}>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <ShortDescription/>
                    </div>
                    <div class="col-lg-8">
                        <PersonalInfo/>
                    </div>
                    <div class="col-lg-8 m-sj">
                        <div class="card shadow-sm">
                            <div class="card-header bg-transparent border-0">
                                <h3 class="mb-0"><i class="far fa-clone pr-1"></i>Personal Information</h3>
                            </div>
                            <div class="card-body pt-0">
                                <table class="table table-bordered">
                                    <tr>
                                        <th width="30%">Father`s Name</th>
                                        <td width="2%">:</td>
                                        <td>Jalal Uddin</td>
                                    </tr>
                                    <tr>
                                        <th width="30%">Mother`s Name </th>
                                        <td width="2%">:</td>
                                        <td>Najma Begum</td>
                                    </tr>
                                    <tr>
                                        <th width="30%">Emergency Contract</th>
                                        <td width="2%">:</td>
                                        <td>01409237654</td>
                                    </tr>
                                    <tr>
                                        <th width="30%">Date of Birth</th>
                                        <td width="2%">:</td>
                                        <td>31/12/1997</td>
                                    </tr>
                                    <tr>
                                        <th width="30%">Blood Group</th>
                                        <td width="2%">:</td>
                                        <td>A+</td>
                                    </tr>
                                    <tr>
                                        <th width="30%">Blood Group</th>
                                        <td width="2%">:</td>
                                        <td>A+</td>
                                    </tr>
                                    <tr>
                                        <th width="30%"></th>
                                        <td width="2%">:</td>
                                        <td>newazs01@gmail.com</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Profile