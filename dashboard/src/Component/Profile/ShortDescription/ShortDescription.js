import React from 'react'

const ShortDescription = () => {
    return (
        <div class="card shadow-sm">
            <div class="card-header bg-transparent text-center">
                <img class="profile_img" src="./icon/tms.png" alt=""/>
                <h3>Zahid Hasan</h3>
            </div>
            <div class="card-body">
                <p class="mb-0"><strong class="pr-1">Student ID:</strong>321000001</p>
                <p class="mb-0"><strong class="pr-1">Course Name:</strong>TMS</p>
                <p class="mb-0"><strong class="pr-1">Section:</strong>A</p>
            </div>
        </div>
    )
}

export default ShortDescription
