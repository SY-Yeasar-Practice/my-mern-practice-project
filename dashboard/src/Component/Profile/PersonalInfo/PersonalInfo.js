import React, {useState, useEffect} from 'react'

const PersonalInfo = () => {
    const [data, setData] = useState([])
    const studentData = [
        {
            position: "Student Id",
            value: 32100001
        },
        {
            position: "Academic Seassion",
            value: 2002
        },
        {
            position: "Gender",
            value: "Male"
        },
        {
            position: "Mobile Number",
            value: "01521427881"
        },
        {
            position: "Email",
            value: "sadmanishopnil@gamil.com"
        }
        
    ]
    useEffect (() => {
        setData(studentData)
    }, [])
    return (
        <div>
            <div class="card shadow-sm">
                <div class="card-header bg-transparent border-0">
                    <h3 class="mb-0"><i class="far fa-clone pr-1"></i>General Information</h3>
                </div>
                <div class="card-body pt-0">
                    {
                        data.length == 0 
                        ? 
                        <>
                            <h1>Loading.....</h1>
                        </>
                        :
                         <table class="table table-bordered">
                            {
                                    data.map (value => {
                                        return (
                                            <tr>
                                                <th width="30%">{value.position}</th>
                                                <td width="2%">:</td>
                                                <td>{value.value}</td>
                                            </tr>
                                        )
                                    })
                                }
                        </table>
                    }
                   
                </div>
            </div>
        </div>
    )
}

export default PersonalInfo
