import React from 'react'

const Attendence = () => {
    return (
        <div style = {{height: "90vh", display: "flex", justifyContent: "center", alignItems: "center"}}>
             <div >
                 <p>Student Attendance System</p>
                <table>
                    <thead>
                        <tr>
                            <th class="name-col">Student Name</th>
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>5</th>
                            <th>6</th>
                            <th>7</th>
                            <th>8</th>
                            <th>9</th>
                            <th>10</th>
                            <th>11</th>
                            <th>12</th>
                            <th class="missed-col">Days Missed-col</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="student">
                            <td class="name-col">Shah Newaz</td>
                            <td class="attend-col"><input type="checkbox"/></td>
                            <td class="attend-col"><input type="checkbox"/></td>
                            <td class="attend-col"><input type="checkbox"/></td>
                            <td class="attend-col"><input type="checkbox"/></td>
                            <td class="attend-col"><input type="checkbox"/></td>
                            <td class="attend-col"><input type="checkbox"/></td>
                            <td class="attend-col"><input type="checkbox"/></td>
                            <td class="attend-col"><input type="checkbox"/></td>
                            <td class="attend-col"><input type="checkbox"/></td>
                            <td class="attend-col"><input type="checkbox"/></td>
                            <td class="attend-col"><input type="checkbox"/></td>
                            <td class="attend-col"><input type="checkbox"/></td>
                            <td class="missed-col">0</td>
                        </tr>

                    </tbody>
                </table>
             </div>
        </div>
    )
}

export default Attendence
