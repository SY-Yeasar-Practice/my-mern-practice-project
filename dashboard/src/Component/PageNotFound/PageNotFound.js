import React from 'react'

const PageNotFound = () => {
    return (
        <div>
            <h1 style = {{marginTop: "8%"}}>Page Not Found</h1>
        </div>
    )
}

export default PageNotFound
