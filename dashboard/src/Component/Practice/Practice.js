import React, { useContext } from 'react'
import { UseSelector } from '../../Context/ProfileContext'

const Practice = () => {
    const student = [
        {
            name: "Sam",
            age: 78
        },
        {
            name: "Sohan",
            age: 22
        },
        {
            name: "Ridom",
            age: 20
        },
        {
            name: "zahid",
            age: 23
        },
        {
            name: "zahid",
            age: 23
        },
        {
            name: "zahid",
            age: 23
        },
        
    ]
    // const data = useContext(UseSelector) 
    // const {name} = data.profile.state
    // const {dispatch} = data.profile
    // console.log(data);
    return (
        <div>
           {
               student.map(value => {
                   console.log(value.age);
                   return (
                        <>
                            <h1>My Name is {value.name} </h1>
                            <h>I am  {value.age} years old</h>
                        </>
                   )
               })
           }
        </div>
    )
}

export default Practice
