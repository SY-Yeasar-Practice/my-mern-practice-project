import React from 'react'
import BreadCrumb from '../BreadCrumb/BreadCrumb'
import MyCourse from '../MyCourse/MyCourse'
import Report from '../Report/Report'
import Satistic from '../Statistic/Satistic'

const Home = () => {
    return (
        <div>
            <BreadCrumb/>
            <Satistic/>
            {/* <Report/> */}
            <MyCourse/>
        </div>
    )
}

export default Home
