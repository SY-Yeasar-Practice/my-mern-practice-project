import React, {useState} from 'react'


const Multiple = ({receiver}) => {
    const [preview, setPreview] = useState([])

    const imageHandler = async (e) => {
        e.preventDefault();
        let finalData = []
        const file = e.target.files
        for (let i = 0 ; i < file.length ; i++) {
            let data = file[i]
            if (data.name) {
                const base64Data = new Promise (resolve => { //to run it as a synchronous promise way and create a new promiser
                    const reader = new FileReader()
                    reader.onloadend = () => {
                        const base64 = reader.result //store the image 
                        resolve (base64)
                    }
                    reader.readAsDataURL (data)
                })
                const fileData = {
                    base64: await base64Data,
                    size: data.size
                }
                finalData.push(fileData)
            }
        }
        receiver (finalData)
        setPreview (finalData)
    }
    return (
        <div>
            <form>
                <div class="form-group">
                    <label for="exampleFormControlFile1">Example file input</label>
                    <input 
                    multiple
                    type="file" 
                    class="form-control-file" 
                    id="exampleFormControlFile1"
                    onChange = {(e) => imageHandler(e)}/>
                </div>
            </form>
            <div>
                {
                    preview.length != 0 
                    &&
                    <>
                        {
                            preview.map (data => {
                                
                                return (
                                    <img src= {data.base64} alt="Mountain View" style= {{width:"304px",height:"228px"}}/>

                                )
                            })
                        }
                    </>
                }
            </div>
        </div>
    )
}

export default Multiple
