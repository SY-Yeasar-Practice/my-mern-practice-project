import React, {useState} from 'react'
import defaultImage from './bg-title-01.jpg'

const Single = ({receiver}) => {
    const [preview, setPreview] = useState("")
    const imageHandler = (e) => {
        e.preventDefault();
        const file = e.target.files[0]
        if (file.name) {
            const reader = new FileReader
            reader.onloadend = () => {
                const base64 = reader.result //store the image 
                setPreview(base64)
                receiver ({base64, size: file.size})
            }
            reader.readAsDataURL (file)
        }
    }
    return (
        <div>
            <form>
                <div class="form-group">
                    <label for="exampleFormControlFile1">Example file input</label>
                    <input 
                    type="file" 
                    class="form-control-file" 
                    id="exampleFormControlFile1"
                    onChange = {(e) => imageHandler(e)}/>
                </div>
            </form>
            <div>
                {
                    preview 
                    &&
                    <img src= {preview} alt="Mountain View" style= {{width:"304px",height:"228px"}}/>
                }
            </div>
        </div>
    )
}

export default Single
