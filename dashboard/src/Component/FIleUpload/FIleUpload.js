import React,{useState} from 'react'
import Single from './Single/Single'
import serverUrl from '../../Server/dataUrl.js'
import axios from 'axios'
import Multiple from './Multiple/Multiple'

const FIleUpload = () => {
    const [singleFile, setSingleFile] = useState("")
    const [multipleFile, setMultipleFile] = useState([])
    const [uploadMessage, setUploadMessage] = useState("")
    const imageReceiveHandler = (data) => {
        setSingleFile (data)
    }
    const imageReceiveHandlerDouble = (data) => {
        setMultipleFile(data)
    }
    const uploadHandler = async (e) => {
        e.preventDefault(); 
        const response = await axios.post (`${serverUrl}/image/upload`, singleFile)
    }   

    const DoubleUploadHandler = async (e) => {
        e.preventDefault(); 
        const response = await axios.post (`${serverUrl}/image/upload`, {file:multipleFile})
    }   
    return (
        <div>
            <div>
                <h1>Single Upload</h1>
                <Single receiver = {imageReceiveHandler}  />
                <button 
                className = {`btn btn-success`}
                onClick = {(e) => uploadHandler(e)}>Upload</button>
                <p>{uploadHandler ? uploadMessage : ""}</p>
            </div>

            <div>
                <h1>Multiple Upload</h1>
                <Multiple receiver = {imageReceiveHandlerDouble}  />
                <button 
                className = {`btn btn-success`}
                onClick = {(e) => DoubleUploadHandler(e)}>Upload</button>
                <p>{uploadHandler ? uploadMessage : ""}</p>
            </div>
        </div>
    )
}

export default FIleUpload
