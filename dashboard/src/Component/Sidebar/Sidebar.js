import React from 'react'
import { Link } from 'react-router-dom'
import SideBarStyles from './SidebarStyle.module.css'

const Sidebar = () => {
    return (
        <div>
             {/* <!-- MENU SIDEBAR--> */}
            <aside className = {`menu-sidebar2 ${SideBarStyles.wrapper}`}>
                <div className = {`logo ${SideBarStyles.logoPartWrapper} `}>
                    <Link to = {"/"}>
                        <img src="/icon/tms.png" width="182px" />
                    </Link>
                </div>
                <div className = {`menu-sidebar2__content js-scrollbar1 `} >
                    <div class="account2">
                        <div class="image img-cir img-120">
                            <img src="/icon/avatar-06.jpg"/>
                        </div>
                        <h4 class="name">Shah Newaz</h4>
                        <a href="#">Sign out</a>
                    </div>
                    {/* <!-- left side navbar --> */}
                    <nav class="navbar-sidebar2">
                        <ul class="list-unstyled navbar__list">
                            {/* <!-- my profile --> */}
                            <li>
                                <Link to="/profile" className = {`${SideBarStyles.menuBarTextColor}`}>
                                    <i class="fas fa-user-alt"></i>My profile</Link>
                                {/* <span class="inbox-num">0</span>  */}
                            </li>

                             {/* Registration  */}
                            <li>
                                <Link to="/registration" className = {`${SideBarStyles.menuBarTextColor}`} >
                                   <i class="fas fa-sign-in-alt"></i>Registration</Link>
                            </li>

                            {/* Attendance  */}
                            <li>
                                <Link to="/attendance" className = {`${SideBarStyles.menuBarTextColor}`} >
                                    <i class="fas fa-shopping-basket"></i>Attendance</Link>
                            </li>
                            {/* <!-- result --> */}
                            <li class="has-sub" className = {`${SideBarStyles.menuBarTextColor}`} >
                                <a class="js-arrow" href="#">
                                <i class="fas fa-poll-h"></i> Result
                                    <span class="arrow">
                                        <i class="fas fa-angle-down"></i>
                                    </span> 
                                </a>
                            </li>
                            {/* <!-- payment --> */}
                            <li class="has-sub" className = {`${SideBarStyles.menuBarTextColor}`} >
                                <Link to = "/payment" class="js-arrow" className = {`${SideBarStyles.menuBarTextColor}`} >
                                    <i class="fas fa-money-bill-alt"></i> Payment
                                </Link>
                            </li>
                        </ul>
                    </nav>
                </div>
            </aside>
        </div>
    )
}

export default Sidebar
