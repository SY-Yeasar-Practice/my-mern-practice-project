import React, {useState} from 'react'
import { BankDeposite } from './BankDeposite/BankDeposite';
import Cash from './Cash/Cash'
import Cheque from './Cheque/Cheque';
import commonStyleSheet from './common.module.css'
import paymentFormStyleSheet from './paymentForm.module.css'

const PaymentForm = () => {
    const [formData, setFormData] = useState({
        paymentType: "cash"
    })
    console.log(formData);
    return (
        <div className = {`row`}>
            <section className = {`${paymentFormStyleSheet.wrapper} col-12 col-md-6`}>
                <form>
                    {/* deposit type */}
                    <div className="form-group">
                        <label for="exampleFormControlSelect1">Deposit Type</label>
                        <select 
                        className= {`form-control ${commonStyleSheet.form_control}`} 
                        id="exampleFormControlSelect1"
                        onChange = {(e) => setFormData({
                            ...formData,
                            paymentType: e.target.value
                        })}>
                            <option selected value = "cash">Cash</option>
                            <option value = "cheque" >Cheque</option>
                            <option value = "bankDeposit" >Bank Deposit/ Rocket payment</option>
                        </select>
                    </div>
                    {
                        formData.paymentType == "cash"
                        &&
                        <Cash/>
                    }

                    {
                        formData.paymentType == "cheque"
                        &&
                        <Cheque/>
                    }

                    {
                        formData.paymentType == "bankDeposit"
                        &&
                        <BankDeposite/>
                    }

                    {/* submit button */}
                    <div className = {`row`}>
                        <div className = {`col-0 col-md-2`}></div>
                        <div className = {`col-12 col-md-10`}>
                            <button style = {{width: "100%"}}type="submit" className="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    )
}

export default PaymentForm

