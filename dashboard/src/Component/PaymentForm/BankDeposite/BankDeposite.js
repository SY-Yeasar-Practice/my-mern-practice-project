import React, {useState}  from 'react'
import Common from '../Common/Common'
import commonStyleSheet from '../common.module.css'

export const BankDeposite = () => {
    const [selectDeposit, setSelectDeposit] = useState (false)
    const depositeHandler = (e) => {
        e.preventDefault();
        if (e.target.value == "bkash" || e.target.value ==  "rocket" ||  e.target.value == "nagad" || e.target.value == "ucash" || e.target.value == "card") {
            setSelectDeposit(true)
        }else {
            setSelectDeposit(false)
        }
    }
    return (
        <>
            <div class="form-group">
                    <label for="exampleFormControlSelect1">Deposit Type</label>
                    <select 
                    className= {`form-control ${commonStyleSheet.form_control}`} 
                    id="exampleFormControlSelect1"
                    onClick = {(e) => depositeHandler(e)}
                    >
                        <option selected value = "cash">Select Deposited Bank A/C</option>
                        <option value = "bkash" >Bkash</option>
                        <option value = "rocket" >Rocket</option>
                        <option value = "nagad" >Nagad</option>
                        <option value = "ucash" >Ucash</option>
                        <option value = "card" >Card</option>
                    </select>
                </div>
                {
                    selectDeposit 
                    && 
                    // A/C  number 
                    <div class="form-group">
                        <label for="exampleFormControlInput1" className =  {`text-uppercase`}>A/C Number</label>
                        <input 
                        type="text" 
                        class="form-control" 
                        id="" 
                        placeholder="A/C Number"/>
                    </div>
                }
                <Common/>
        </>
    )
}
