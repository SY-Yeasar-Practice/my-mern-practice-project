import React from 'react'
import Common from '../Common/Common'
import commonStyleSheet from '../common.module.css'

const Cash = () => {
    return (
        <>
            <Common/>
            {/* Remarks0 */}
             <div class="form-group">
                <label for="exampleFormControlTextarea1" className =  {`text-uppercase `}>Remarks</label>
                <textarea  className= {`form-control ${commonStyleSheet.form_control}`}  id="exampleFormControlTextarea1" placeholder="Remare" rows="1"></textarea>
            </div>
        </>
    )
}

export default Cash
