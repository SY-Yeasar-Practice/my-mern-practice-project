import React from 'react'
import Common from '../Common/Common'
import commonStyleSheet from '../common.module.css'

const Cheque = () => {
    return (
       <>
            {/* check no */}
            <div class="form-group">
                <label for="exampleFormControlInput1" className =  {`text-uppercase`}>check no</label>
                <input 
                type="text" 
                className= {`form-control ${commonStyleSheet.form_control}`} 
                id="" 
                placeholder="Check Number"/>
            </div>

            {/* check bank # */}
            <div class="form-group">
                <label for="exampleFormControlInput1" className =  {`text-uppercase`}>check bank</label>
                <input 
                type="text" 
                className= {`form-control ${commonStyleSheet.form_control}`}  
                id="" 
                placeholder="Check Bank"/>
            </div>
            {/* check issue date */}
               <div className="mb-3">
                    <label for="exampleInputEmail1" className={`text-uppercase form-label `} >
                        check issue date
                    </label>
                    <input
                        type="date"
                        className= {`form-control ${commonStyleSheet.form_control}`} 
                        id="exampleInputEmail1"
                        aria-describedby="emailHelp"
                    ></input>
                </div>
                <Common/>
            {/* appointment attachements */}
            <div class="input-group mb-3">
                <label for="exampleFormControlInput1" className =  {`text-uppercase`}>attachment</label>
                <label for="exampleFormControlInput1" className =  {`text-uppercase btn btn-primary  ${commonStyleSheet.form_control}  ${commonStyleSheet.attachment}`}>Select image</label>
                <input style = {{visibility:"hidden", display : "none"}} type="file" class="form-control" id="exampleFormControlInput1" aria-describedby="inputGroupFileAddon03" aria-label="Upload"/>
            </div>
        </>
    )
}

export default Cheque
