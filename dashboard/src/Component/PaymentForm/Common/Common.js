import React from 'react'
import commonStyleSheet from '../common.module.css'

const Common = () => {
    return (
        <>
             {/* Exchange rates */}
            <div class="form-group">
                <label for="exampleFormControlInput1" className =  {`text-uppercase`}>exchange rate</label>
                <input 
                type="text" 
                className= {`form-control ${commonStyleSheet.form_control}`}  
                id="" 
                placeholder="1000000"/>
            </div>

            {/* Refferance # */}
            <div class="form-group">
                <label for="exampleFormControlInput1" className =  {`text-uppercase`}>reference#</label>
                <input 
                type="text" 
                className= {`form-control ${commonStyleSheet.form_control}`} 
                id="" 
                placeholder="Reference number"/>
            </div>

            {/* Amount # */}
            <div class="form-group">
                <label for="exampleFormControlInput1" className =  {`text-uppercase`}>amount(bdt)</label>
                <input 
                type="text" 
                className= {`form-control ${commonStyleSheet.form_control}`}  
                id="" 
                placeholder="0"/>
            </div>
        </>
    )
}

export default Common
