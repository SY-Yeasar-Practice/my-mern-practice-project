import React, {useState} from 'react'
import CourseRegistrationStyleSheet from './CourseRegister.module.css'

const CourseRegistration = () => {
    //course registration from data
    const [registrationFormData, setRegistrationFormData] = useState([
        {
            courseId: "cse412",
            sec: "A",
            time: "8am -9.30pm",
            enrolled: "39",
            cap: "40",
            courseName : "Internship"
        },
        {
            courseId: "cse412",
            sec: "A",
            time: "8am -9.30pm",
            enrolled: "39",
            cap: "40",
            courseName : "Internship"
        },
        {
            courseId: "cse412",
            sec: "A",
            time: "8am -9.30pm",
            enrolled: "39",
            cap: "40",
            courseName : "Internship"
        },
        {
            courseId: "cse412",
            sec: "A",
            time: "8am -9.30pm",
            enrolled: "39",
            cap: "40",
            courseName : "Internship"
        },
        {
            courseId: "cse412",
            sec: "A",
            time: "8am -9.30pm",
            enrolled: "39",
            cap: "40",
            courseName : "Internship"
        },
    ])
    const [enrolledData, setEnrolledData] = useState ([])

    const addCourseHandler = (e, data, ind) => {
        e.preventDefault(); 
        const check = enrolledData.filter (data => data.ind == ind)
        if (check.length == 0) {
            setEnrolledData ([...enrolledData, {...data, ind:ind}])
        }
    }
    return (
        <div className= {`bg-success p-4 pb-5 ${CourseRegistrationStyleSheet.text}`}>
            {/* <!-- main wraper --> */}
            <div className="row" style = {{paddingTop: "8%"}}>
                {/* <!-- course wraper --> */}
                <div class="col-12 col-lg-7">
                    <caption className = {`${CourseRegistrationStyleSheet.text} d-block text-center `}>Course Regestration</caption>
                    <table>
                        <thead>
                            <tr>
                                <th scope="col">Course </th>
                                <th scope="col">sec</th>
                                <th scope="col">Time</th>
                                <th scope="col">Enrolled</th>
                                <th scope="col">Cap</th>
                                <th scope="col">Course Add</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                registrationFormData.map ((data, ind) => {
                                    return (
                                        <tr key = {ind}>
                                            <td data-label="Course">{data.course}</td>
                                            <td data-label="sec">{data.sec}</td>
                                            <td data-label="time">{data.time}</td>
                                            <td data-label="Enrolled">{data.enrolled}</td>
                                            <td data-label="cap">{data.cap}</td>
                                            {/* <td><button class="course" onClick = {(e) => console.log(data)}> Add Course </button> </td> */}
                                            <td> <button class=" btn btn-primary" onClick = {(e) => addCourseHandler(e, data, ind)} >Click me</button></td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                </div>
                {/* <!-- enrolled wraper --> */}
                <div className = {`col-12 col-lg-5 d-flex`}  >
                    <div style = {{display: "flex" , justifyContent: "center" , alignItems: "center"}} >
                        <div className="" >
                            <caption className = {`${CourseRegistrationStyleSheet.text} d-block text-center`}>Enroll course</caption>
                            <table>
                                <thead>
                                    <tr>
                                        <th scope="col">Course Id</th>
                                        <th scope="col">Course Name</th>
                                        <th scope="col">Course Timing</th>
                                        <th scope="col">Course Section</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        enrolledData.map((data, ind) => {
                                            return (
                                                 <tr key = {ind}>
                                                    <td data-label="Course ID">{data.courseId}</td>
                                                    <td data-label="Course Name">{data.courseName}</td>
                                                    <td data-label="Course Timing">{data.time}</td>
                                                    <td data-label="Course Section">{data.sec}</td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    )
}

export default CourseRegistration
