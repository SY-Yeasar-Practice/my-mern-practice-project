import React from 'react'

const MyCourse = () => {
    return (
        <div className = {`d-flex justify-content-center align-center`} >
           <div >
                <caption style = {{display: "block"}}>My course</caption>
                <table>
                    <thead>
                        <tr>
                            <th scope="col">Course Id</th>
                            <th scope="col">Course Name</th>
                            <th scope="col">Course Timing</th>
                            <th scope="col">Course Section</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-label="Course ID">cse412</td>
                            <td data-label="Course Name">Internship pogram</td>
                            <td data-label="Course Timing">8am -9.30pm</td>
                            <td data-label="Course Section">A</td>
                        </tr>

                    </tbody>
                </table>
           </div>
        </div>
    )
}

export default MyCourse
