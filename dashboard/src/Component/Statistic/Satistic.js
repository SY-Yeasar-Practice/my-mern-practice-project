import React from 'react'

const Satistic = () => {
    return (
        <div>
             {/* <!-- STATISTIC--> */}
            <section class="statistic">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="statistic__item">
                                    <h2 class="number">0</h2>
                                    <span class="desc">attendance</span>
                                    <div class="icon">
                                        <i class="zmdi zmdi-account-o"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="statistic__item">
                                    <h2 class="number">Average</h2>
                                    <span class="desc">Result</span>
                                    <div class="icon">
                                    <i class="fas fa-poll-h"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default Satistic
