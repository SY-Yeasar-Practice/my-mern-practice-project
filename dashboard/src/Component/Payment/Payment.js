import React from 'react'
import PaymentForm from '../PaymentForm/PaymentForm'
import Amount from './Amount/Amount'

const Payment = () => {
    return (
        <div className = {`row`} style = {{marginTop: "12%", marginLeft: "6%"}}>
            <div className = {`col-12 col-md-7`}>
                <PaymentForm/>
            </div>
            <div className = {`col-12 col-md-5`} style = {{display: "flex", justifyContent: "center", alignItems: "center",background: "#95989B"}} >
                <Amount/>
            </div>
        </div>
    )
}

export default Payment
