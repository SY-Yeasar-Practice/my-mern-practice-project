import axios from 'axios'
import React, {useState, useEffect} from 'react'

const NearestShop = () => {
    const [showRecent, setRecent] = useState(false)
    const [shopData, setShopData] = useState([])
    const [myLocation , setMyLocation] = useState ({
        lang: "",
        lat: ""
    })
    const nearestShopHandler = (e) => {
        e.preventDefault(); 
        setRecent(!showRecent)
    }
    console.log(shopData);
    useEffect (() => {
        return (async () => {
            if (showRecent) {
                const location = new Promise ((resolve, reject) => {
                    navigator.geolocation.getCurrentPosition(position => {
                        resolve ( {
                            lang: position.coords.latitude,
                            lat: position.coords.longitude
                        })
                    })
                })
                const currentPosition = await location
                const response = await axios.post("http://localhost:3030/shop/nearest", currentPosition)
                if (response.status == 202) {
                    setShopData (response.data.data)
                }else {
                    setShopData([])
                }

            }else {
                setShopData([])
            }
        })()
    }, [showRecent])
    return (
        <div className = {`container`}>
           <h4>Show Nearest Shop 
                <button onClick = {(e) => nearestShopHandler (e)}><i className = {showRecent ? "far fa-times-circle" : "fas fa-location-arrow"} ></i></button>
           </h4>
           {
               shopData.length == 0 
               ?
               <h1>No Shop found</h1>
               :
               <>
                {
                    shopData.map (data => {
                        return (
                            <h1>{data.name}</h1>
                        )
                    })
                }
               </>
           }
        </div>
    )
}

export default NearestShop
