import React, {useState} from 'react'
import axios from 'axios'

const MyLocation = () => {
    const [myLocation, setMyLocation ] = useState({
        longitude: "",
        latitude: "",
    })
    const [shopInfo, setShopInfo] = useState({
        name: "",
        desc: "",
        lat: "",
        lang: ""
    })
    const [message, setMessage] = useState("")
    const locationHandler = (e) => {
        e.preventDefault()
        navigator.geolocation.getCurrentPosition(position => {
            const location = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
            }
            

            setMyLocation(location)
        })
    }
    const createShopHandler = async (e) => {
        e.preventDefault();
        const data = {
            name: shopInfo.name,
            description: shopInfo.desc,
            longitude: shopInfo.lang,
            latitude: shopInfo.lat
        }
        const create = await axios.post ("http://localhost:3030/shop/create", data) 
        if (create.status == 201) {
            setMessage (create.data.message)
        }else {
            setMessage (create.data.message)
        }
    }
    console.log(shopInfo);
    return (
        <div className = {`container`}>
            <form>
                <div className="form-group">
                    <label for="exampleInputEmail1">Shop Name</label>
                    <input 
                    type="text" 
                    className="form-control" 
                    id="exampleInputEmail1" 
                    aria-describedby="" 
                    placeholder="Shop Name"
                    onChange = {(e) => setShopInfo({
                        ...shopInfo, 
                        name: e.target.value
                    })}/>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Description</label>
                    <textarea 
                    className="form-control" 
                    id="exampleFormControlTextarea1" 
                    rows="3"
                    onChange = {(e) => setShopInfo({
                        ...shopInfo, 
                        desc: e.target.value
                    })}></textarea>
                </div>

                <div className="form-group">
                    <label for="exampleInputEmail1">Longitude</label>
                    <input 
                    type="text" 
                    className="form-control" 
                    id="" 
                    aria-describedby="" 
                    placeholder="Longitude"
                    onChange = {(e) => setShopInfo({
                        ...shopInfo, 
                        lang: e.target.value
                    })}/>
                </div>

                <div className="form-group">
                    <label for="exampleInputEmail1">Latitude</label>
                    <input 
                    type="text" 
                    className="form-control" 
                    id="exampleInputEmail1" 
                    aria-describedby="" 
                    placeholder="Latitude"
                    onChange = {(e) => setShopInfo({
                        ...shopInfo, 
                        lat: e.target.value
                    })}/>
                </div>
                <button 
                type="submit" 
                className="btn btn-primary"
                onClick = {(e) => createShopHandler(e) }>Create Shop</button>
                {
                    message
                    &&
                    <p>{message}</p>
                }
                </form>
        </div>
    )
}

export default MyLocation
