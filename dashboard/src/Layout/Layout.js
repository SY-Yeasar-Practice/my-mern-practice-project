import React from 'react'
import { Route, Switch } from 'react-router'
import BreadCrumb from '../Component/BreadCrumb/BreadCrumb'
import Footer from '../Component/Footer/Footer'
import Home from '../Component/Home/Home'
import NavBar from '../Component/NavBar/NavBar'
import PageNotFound from '../Component/PageNotFound/PageNotFound'
import Profile from '../Component/Profile/Profile'
import Report from '../Component/Report/Report'
import Sidebar from '../Component/Sidebar/Sidebar'
import Satistic from '../Component/Statistic/Satistic'
import CourseRegistration from  '../Component/CourseRegistration/CourseRegistration'
import Attendence from '../Component/Attendence/Attendence'
import Payment from '../Component/Payment/Payment'

const Layout = () => {
    return (
        <div>
            <div className = "page-wrapper row" >
                <div className = "col-12 col-md-2">
                    <Sidebar/>
                </div>
                <NavBar className = "col-12"/>
                <div class = "page-container2 col-12 col-md-10" style = {{paddingLeft: "5%"}}> 
                    <Switch>
                        <Route exact component= {Home} path = {`/`}/>
                        <Route exact component= {Profile} path = {`/profile`}/>
                        <Route exact component= {CourseRegistration} path = {`/registration`}/>
                        <Route exact component= {Attendence} path = {`/attendance`}/>
                        <Route exact component= {Payment} path = {`/payment`}/>
                        <Route path= "*" component={PageNotFound} />
                    </Switch>
                    <Footer/>
                </div>
            </div>
        </div>
    )
}

export default Layout
