import React, { useReducer } from 'react'

const initialState = {
    name: "shopnil"
}
const profileReducer = (state = initialState, action)  => {
    switch (action.type){
        case "ADD" : {
            return {
                ...state,
                name: action.payload
            }
        }
    }
}
export  {
    profileReducer,
    initialState
}