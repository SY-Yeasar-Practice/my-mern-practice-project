import logo from './logo.svg';
import './App.css';
import Layout from './Layout/Layout';
import {BrowserRouter} from 'react-router-dom'
import Practice from './Component/Practice/Practice';
import { Providers } from './Context/ProfileContext';
import { useReducer, useEffect } from 'react';
import {initialState, profileReducer} from './Store/profile/reducer'
import axios from 'axios'
import url from './Data/User/user'
import Profile from './Component/Profile/Profile';
import MyLocation from './Component/MyLocation/MyLocation';
import NearestShop from './Component/MyLocation/NearestShop';
import PaymentForm from './Component/PaymentForm/PaymentForm';
import CourseRegistration from './Component/CourseRegistration/CourseRegistration';
import Attendence from './Component/Attendence/Attendence';
import Payment from './Component/Payment/Payment'
import FIleUpload from './Component/FIleUpload/FIleUpload';

function App() {
  const [profileState, profileDispatch] = useReducer(profileReducer, initialState)
  const url = "./Data/User/user.json"
  //create the store
  const store = {
    profile: {
      state: profileState,
      dispatch: profileDispatch
    }
  }
  // async function func () {
  //     try {
  //       const data = await fetch(url);
  //       const response = await data.json();  
  //       console.log(response);
  //     } catch (error) {
  //       console.log(error);
  //     }
  // }
  // func()
  // useEffect(() => {

  // }, [])
  return (
    <div className="">
      <BrowserRouter>
        <Providers value = {store}>
           {/* <Layout/> */}
           {/* <CourseRegistration/> */}
           {/* <Practice/> */}
           {/* <Profile/> */}
           {/* <MyLocation/> */}
           {/* <NearestShop/> */}
           {/* <PaymentForm/> */}
           {/* <Attendence/> */}
           {/* <Payment/> */}
           <FIleUpload/>
        </Providers>
      </BrowserRouter>
      
    </div>
  );
}

export default App;
