import React  from 'react'

const profileContext = React.createContext() 

const UseSelector = profileContext.Consumer
const Providers = profileContext.Provider

export {
    Providers,
    UseSelector
}
