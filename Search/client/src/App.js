import logo from './logo.svg';
import './App.css';
import DemoOne from './component/DemoOne/DemoOne';
import DemoTwo from './component/DemoTwo';
import ToDoList from './component/toDoList/ToDoList';
import Counter from './component/Counter/Counter';
import React,{useState} from 'react'
import Cleanup from './component/Cleanup/Cleanup';

function App() {
  const [toggle, setToggle] = useState(false)
  return (
    <>
    <Cleanup/>
      {/* <DemoOne/>
      <ToDoList/>
      {
        toggle &&   <Counter/>
      }
      <button onClick={(e) => setToggle(!toggle)}>Click me to hide Counter</button> */}
    </>
  )
}

export default App;
