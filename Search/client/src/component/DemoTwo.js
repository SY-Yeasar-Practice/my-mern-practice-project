import React, { Component } from 'react'

class DemoTwo extends React.PureComponent {
    constructor(props) {
        super()
        this.state = {
            count : 0
        }
    }
    handler = (e) => {
        e.preventDefault()
        this.setState({count : this.state.count + 1 })
    }
    shouldComponentUpdate (nextState, nextProps) {
        if (nextState.count != this.state.count) {
            return true
        }else {
            return false
        }
    }
    componentDidMount () {
        console.log(`Hello I am from component did mount of demo two`);
    }
    componentDidUpdate () {
        console.log(`Hello I am from component did update of demo two`);
    }
    render() {
        console.log(`Hello I am from Demo Two`);
        return (
            <div>
                <h1>Counter 2: {this.state.count}</h1>
                <button onClick = {(e) => this.handler(e) }>Click me</button>
            </div>
        )
    }
}
// export default React.memo(DemoTwo)
export default DemoTwo
