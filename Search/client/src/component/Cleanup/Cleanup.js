import React, { useEffect, useState } from 'react'

function Cleanup() {
    const [width, setwidth] = useState(window.screen.width);
    
    const actualWidth=()=>{
        console.log(window.innerWidth);
        setwidth(window.innerWidth);
    }
    useEffect(()=>{
       window.addEventListener('resize' ,actualWidth);
      return ()=>{
          window.removeEventListener('resize',actualWidth);
      }
    },)
    return (
        <div>
            <h1>cleanup function</h1>
            <p>{width}</p>
        </div>
    )
}

export default Cleanup
