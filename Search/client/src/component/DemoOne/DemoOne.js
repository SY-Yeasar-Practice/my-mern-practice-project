import React, { Component } from 'react'
import DemoTwo from '../DemoTwo'

export default class DemoOne extends  React.PureComponent  {
    constructor(props) {
        super()
        this.state = {
            count : 0
        }
    }
    shouldComponentUpdate (nextState, nextProps) {
        if (nextState.count != this.state.count) {
            return true
        }else {
            return false
        }
    }
    handler = (e) => {
        e.preventDefault()
        this.setState({count : this.state.count + 1 })
    }
    componentDidMount () {
        console.log(`Hello I am from component did mount of demo one`);
    }
    render() {
        console.log(`Hello I am from Demo One`);
        return (
            <div>
                <h1>Counter 1: {this.state.count}</h1>
                <button onClick = {(e) => this.handler(e) }>Click me</button>
                <DemoTwo/>
            </div>
        )
    }
}

