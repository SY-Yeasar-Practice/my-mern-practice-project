import React, {useState, useEffect}from 'react'

const Counter = () => {
    const [counter, setCounter] = useState(0)
    useEffect(() => {
        let counter = setInterval(() => {
            setCounter(counter => counter + 1)
        }, 1000)
        return () => {
            console.log(`I am executed`);
            clearInterval(counter)
        }
    }, [])
    return (
        <div>
            <h1>Auto Counter: {counter}</h1>
        </div>
    )
}

export default Counter
