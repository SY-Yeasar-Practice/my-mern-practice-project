import React, {useState} from 'react'

const ToDoList = () => {
    const initialState = {
        value: "",
        ind: null
    } //create a  initial state 
    const [inputValue, setInputValue] = useState(initialState) //input state
    const [displayValue, setDisplayValue] = useState([]) //display state 
    const changeHandler = (e) => {
        e.preventDefault();
        setInputValue(initialState) 
        setDisplayValue([...displayValue, inputValue.value])
    }
    const editHandler = (e, value, ind) => {
        e.preventDefault(); 
        setInputValue({ value, ind}) //show it in input fill
        
    }
    const updateHander = (e) => {
        e.preventDefault();
        const {ind, value} = inputValue //distructure the value from input
        const newValue = [...displayValue] //copy the existing array
        newValue[ind] = value //update the array element by index
        setDisplayValue(newValue) //set the new display value
    }
    
    const deleteHandler = (e, ind) => {
        e.preventDefault();
        const updatedDisplayValue = [...displayValue] //copy the existing array
        updatedDisplayValue.splice(ind, 1)  //remove the individual value by index
        setDisplayValue(updatedDisplayValue) //set the new value
    }
    return (
        <div>
            {/* input part */}
            <div>
                <input 
                type="text" 
                onChange = {(e) => setInputValue({...inputValue, value: e.target.value})}
                value = {inputValue.value}/>
                <button onClick = {(e) => changeHandler(e)}>Add</button>
                <button onClick = {(e) => updateHander(e)}>Update</button>
            </div>
            
            {/* display part */}
            <div>
                {
                    displayValue.map((value, ind) => {
                        return (
                            <div style = {{marginBottom: "10px"}} key = {ind}>
                                <span>{ind + 1 }. </span>
                                <span>{value}</span>
                                <button style = {{marginLeft: "2%"}} onClick = {(e) => editHandler (e, value, ind)}>Edit</button>
                                <button style = {{marginLeft: "2%"}} onClick = {(e) => deleteHandler(e, ind) }>Delete</button>
                                <br />
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default ToDoList
