const e = require('express')
const Product = require('../model/product')

//create a new product 
const createNewProductController = async (req, res) => {
    try {
        const newProduct  = new Product(req.body) //create a instance of product  
        const saveNewProduct = await newProduct.save() //save a new product 
        if (saveNewProduct) {
            res.status(201).json ({
                message: "New Product has been created",
                data: saveNewProduct
            })
        }else {
            res.json ({
                message: "Product created failed"
            })
        }
    }catch(err) {
        console.log(err);
        res.json({
            message: err.message,
            err
        })
    }
}

//filter the product product 
const filterProductController = async (req, res) => {
    try {
        const {max, min,  category} = req.query //get the data from query 
        const productMaxPrice = max ? max : ""  //set the default value of product maximum price 
        const productMinPrice = min ? min : ""  //set the default value of product minimum price 
        const productCategory = category ? category : ""  //set the default value of product category 
        const query = (productMaxPrice ||  productMinPrice || productCategory )
        ?
        {
            $and: []
        }
        :
        {}
        if (max) {
            query.$and.push({
                "productDetails.priceDetails.current": {
                    $lte: max
                }
            })
        }
        if (min) {
            query.$and.push({
                "productDetails.priceDetails.current": {
                    $gte: min
                }
            })
        }

        if (category) {
            query.$and.push({
                "productDetails.category": category
            })
        }

        const findProduct = await Product.find(query) 
        if (findProduct) {
            res.status(202).json ({
                message: `${findProduct.length} product found`,
                product: findProduct,
            })
        }else {
            res.json({
                message: "Product Not found"
            })
        }
    }catch(err) {
        console.log(err);
        res.json ({
            message: err.message,
            err
        })
    }
}

//product search by word 
const productSearchController = async (req, res) => {
    try {
        const {search} = req.body //get the search text from body 
        console.log(search);
        const findProduct = await Product.find({
            $text: {
                $search: search
            }
        })
        if (findProduct) {
            res.status(202).json({
                message: "Product found",
                data: findProduct
            })
        }else {
            res.json({
                message: "Product not found"
            })
        }
    }catch (err) {
        console.log(err);
        res.json({
            message: err.message,
            err
        })
    }
}

//export part 
module.exports = {
    createNewProductController,
    filterProductController,
    productSearchController
}

