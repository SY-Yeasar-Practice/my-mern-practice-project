const User = require('../model/user')

const createUserController = async (req, res) => {
    try {
        const newUser = new User (req.body) //create a new user 
        const saveNewUser = await newUser.save()   //save a new user 
        if (saveNewUser) {
            res.status(201).json({
                message: "New User Created",
                data: saveNewUser
            })
        }else {
            res.json({
                message: "User created failed"
            })
        }
    }catch (err) {
        console.log(err);
        res.json({
            message: err.message
        })
    }
}

//search by individual word
const searchPersonController = async (req, res) => {
    try {
        const {search} =  req.body //get the search input  form the body 
        const splitData = search.split(" ").filter(data => data != ''); //split all search data by a space and get individual word
        let searchReg = "" //this is initial value of search regex
        splitData.map((data, ind) => {
            if (splitData.length > ind + 1) { 
                searchReg += data + "|" //use the or logic of regex to search individual words
            }else {
                searchReg += data //this is for the last word of the array
            }
        }) //here search input's individual word will store as a or regex expression
        const searchRegex = new RegExp(searchReg , "ig") //search individual word regex 
        const findData = await User.find({
            $or: [
                {
                    firstName: searchRegex
                },
                {
                    lastName: searchRegex
                }
            ]
        })
        if (findData) {
            res.status(202).json({
                message: `${findData.length} users found`,
                data: findData
            })
        }else {
            res.json({
                message: `No data found`
            })
        }
    }catch (err) {
        console.log(err);

        
        res.json({
            message: err.message
        })
    }
}

//get user
const getUser = async (req, res) => {
    try{
        const data = ["Uddin", "Rahma"]
        const findUser = await User.find({
            lastName: data
        })
        console.log(findUser);
    }catch(err) {
        console.log(err);
    }
}

module.exports = {
    createUserController,
    searchPersonController,
    getUser
}

const signin=(email,password)=>async(dispatch)=>{
    dispatch({type:USER_SIGNIN_REQUEST,payload:{email,password}});
    try {
      
        
        const { data } = await axios.post('http://localhost:4000/api/users/signin',{email,password});
       
        dispatch({type:USER_SIGNIN_SUCCESS,payload:data})
     
        cookie.set('userInfo',JSON.stringify(data))
    } catch (error) {
        dispatch({ type: USER_SIGNIN_FAIL, payload: error.message })
    }
}