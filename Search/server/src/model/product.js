const mongoose = require('mongoose')
const Schema = mongoose.Schema

const productSchema = new Schema ({
    productDetails : {
        name: {
            type: String
        },
        priceDetails: {
            current : {
                type: Number,
                default: 0
            }
        },
        description: String,
        category: String,
        tag: [String]
    }
},{
    timestamps: true
})
productSchema.index({ "productDetails.name": 'text' });

module.exports = mongoose.model("Product", productSchema)