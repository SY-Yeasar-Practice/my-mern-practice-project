const express = require('express');
const route = express.Router() ; 
const {createNewProductController,
    filterProductController,
    productSearchController} = require('../controller/product')

route.post("/create", createNewProductController)

route.get("/filter", filterProductController)
route.get("/search", productSearchController)

module.exports  = route