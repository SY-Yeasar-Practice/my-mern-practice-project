const express = require('express');
const route = express.Router() ; 
const {sentMessageController} = require('../controller/twiloTest')

route.get("/sent/message", sentMessageController)

module.exports = route;