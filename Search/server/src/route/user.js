const express = require('express');
const route = express.Router() ; 
const {createUserController,
    searchPersonController,
    getUser} = require('../controller/user')

route.post("/create", createUserController)

route.get("/search", searchPersonController)
route.get("/user", getUser)

module.exports  = route